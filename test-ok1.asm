
main:
		PUSH	%14
		MOV 	%15,%14
		SUBS	%15,$48,%15
@main_body:
		MOV 	$1,-44(%14)
		MOV 	$13,-40(%14)
		MOV 	 -40(%14),%0
		MOV 	 -44(%14),%1
		ADDS	%0,%1,%0
		ADDS	%0,$4,%0
		MOV 	%0,-48(%14)
@try_1:
		JMP	@catch_1
@catch_1:
		MOV 	$10,-28(%14)
		MOV 	 -28(%14),%0
		ADDS	%0,-48(%14),%0
		MOV 	%0,-4(%14)
		MOV 	-4(%14),%13
		JMP 	@main_exit
@main_exit:
		MOV 	%14,%15
		POP 	%14
		RET