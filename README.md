

PROJEKAT PROGRAMSKI PREVODIOCI 2022 

Tema: try catch iskaz i nizovi

Alati koji su korisceni:
	1. Flex
	2. Bison
	3. Hipsim

Uputstvo za pokretanje...

Potrebno je pozicionirati se u /programski-prevodioci-projekat, a potom pozvati
komandu make test TEST="{putanja"} u ovom slucaju putanja do testova je
"./tests/*" zvezdicu na kraju stavljamo ukoliko zelimo da pokrenemo sve testove
koji su na raspolaganju(ok, sintaksni, semanticki). U suprotnom, moguce je pokrenuti
svaki test za sebe, navodjenjem konkretne putanje do testa u {putanja}.
