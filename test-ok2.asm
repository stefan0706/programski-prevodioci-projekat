
main:
		PUSH	%14
		MOV 	%15,%14
		SUBS	%15,$24,%15
@main_body:
		MOV 	$5,-24(%14)
		MOV 	$11,-16(%14)
		MOV 	 -16(%14),%0
		ADDS	-24(%14),%0,%0
		MOV 	%0,-24(%14)
		MOV 	-24(%14),%13
		JMP 	@main_exit
@main_exit:
		MOV 	%14,%15
		POP 	%14
		RET