//OPIS: Sve iz specifikacije u jednom primeru
//      inicijalizacija niza, dodela vrednosti el. niza, pristupanje el niza
//      try catch blok
//RETURN: 28
int main(int p) {

    int c;
    int a[10];
    int b;


    a[9] = 1;

    a[8] = 13;

    b = a[8] + a[9] + 4; 
    
    try {
        a[13] = 1;

    } catch (error){
        a[5] = 10;
        c = a[5] + b;
    } 

    return c;


}

  
