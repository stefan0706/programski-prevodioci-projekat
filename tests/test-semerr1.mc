//OPIS: pristupanje elementu niza na indeksu koji je veci od duzine samog niza
//RETURN: 10
int main() {
    int a[10];

    int b;

    b = 7 + a[15];

    return 10;
}
