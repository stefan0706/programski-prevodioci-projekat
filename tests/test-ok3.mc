//OPIS: Try catch iskaz za nizove
//RETURN: 7
int main() {
    int b[10];

    try{
        b[19] = 1;
    }
    catch(error1){
        b[1] = 7;
    }

    return b[1];
}
