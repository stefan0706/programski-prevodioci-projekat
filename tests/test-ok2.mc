//OPIS: inicijalizacija niza, dodela vrednosti i pristupanje elementima niza
//RETURN: 16
int main() {
    int a[5];
    int c;

    c = 5;
    a[3] = 11;

    c = c + a[3];

    return c;
}
